package vnlab.com;

/**
 * Created by Minh NC on 5/29/2017.
 */
public class BatchConsumer {
    public static void main(String[] args) {
        Consumer consumerThread = new Consumer(KafkaProperties.TOPIC);
        consumerThread.start();
    }
}
