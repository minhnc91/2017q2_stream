package vnlab.com;

/**
 * Created by Minh NC on 5/29/2017.
 */
public class KafkaProperties {
    public static final String TOPIC = "pageviews_test";
    public static final String KAFKA_SERVER_URL = "vnlab-master1.com";
    public static final int KAFKA_SERVER_PORT = 6667;

    private KafkaProperties() {}
}
