package vnlab.com

import kafka.serializer.StringDecoder
import org.apache.commons.lang.time.FastDateFormat
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SQLContext
import org.apache.spark.streaming._
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka.KafkaUtils


/**
  * Created by Minh NC on 5/26/2017.
  */
case class Pv(time: String, count: Long)

object StreamTest {
  val YYYYMMDDHHMMSS = FastDateFormat.getInstance("yyyyMMddHHmmss")
  val YYYYMMDDHHMM = FastDateFormat.getInstance("yyyyMMddHHmm")

  def main(args: Array[String]) {
    val sparkConf = new SparkConf().setAppName("StreamTest")
    val ssc = new StreamingContext(sparkConf, Seconds(2))
    ssc.checkpoint("checkpoint")

    val topicsSet = Set(KafkaProperties.TOPIC)
    val brokers = KafkaProperties.KAFKA_SERVER_URL + ":" + KafkaProperties.KAFKA_SERVER_PORT
    val kafkaParams: Map[String, String] = Map("metadata.broker.list" -> brokers, "group.id" -> "spark_streaming")
    val messages: InputDStream[(String, String)] = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](ssc, kafkaParams, topicsSet)
    val lines: DStream[String] = messages.map { case (key, message) => message } // .map(_._2)

    val pvs = lines.map(_.split(" "))
    val pvCounts = pvs.map(x => (x(0), 1L)).reduceByKeyAndWindow(_ + _, _ - _, Minutes(10), Seconds(2), 2)
    pvCounts.print()

    // Save
    val current = System.currentTimeMillis()
    val currentM = YYYYMMDDHHMM.format(current).toLong
    val currentS = YYYYMMDDHHMMSS.format(current).toLong

    if (currentS % 10 == 0 && currentM % 10 == 0) {
      print("xxx", currentM, currentS)
      val pvCountsSave = pvs.map(x => (x(0), 1L)).reduceByKeyAndWindow(_ + _, _ - _, Minutes(10), Seconds(2), 2)
        .map(p => Pv(p._1, p._2))

      pvCountsSave.foreachRDD { rdd =>
        val sqlContext = SQLContext.getOrCreate(rdd.sparkContext)
        import sqlContext.implicits._

        val pvsDataFrame = rdd.toDF()

        pvsDataFrame.registerTempTable("pvs")

        val wordCountsDataFrame = sqlContext.sql("select * from pvs")
        wordCountsDataFrame.show()
      }
    }

    ssc.start()
    ssc.awaitTermination()
  }

}
